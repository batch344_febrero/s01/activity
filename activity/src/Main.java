import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = myObj.nextLine();

        System.out.println("Last Name:");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade:");
        double num1 = myObj.nextDouble();

        System.out.println("Second Subject Grade:");
        double num2 = myObj.nextDouble();

        System.out.println("Third Subject Grade:");
        double num3 = myObj.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        int wholeNumber = (int) (num1 + num2 + num3) / 3;
        System.out.println("Your grade average is: " + wholeNumber);

    }
}